import React from 'react';
import './User.css';
import './App.css';
import history from '../history'
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';


class User extends React.Component{
    state = {
        columnDefs: [{
          headerName: "ID", field: "id"
        }, {
          headerName: "Email", field: "email"
        }, {
          headerName: "First Name", field: "first_name"
        },{
          headerName: "Last Name", field: "last_name"
        }],
      }
      componentDidMount() {
    const apiUrl = ' https://reqres.in/api/users';
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => this.setState({rowData:data.data}));
}

    render(){
        return(
            <div>
              <div>
                <h1 className="Login-header">Users</h1>
                <div ClassName="Table-Container">
                <div className="ag-theme-alpine Table-block" style={{height: '300px',width: '825px' }}>
                <AgGridReact
        columnDefs={this.state.columnDefs}
        rowData={this.state.rowData}>
      </AgGridReact>

      </div>
                </div>
              </div>
          </div>
      );

    }
}

export default User;
