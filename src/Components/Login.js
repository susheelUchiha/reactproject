import React from 'react';
// import User from './Users.js'
import './App.css';
import history from '../history'
import {connect} from 'react-redux';



class Login extends React.Component {

        state={
          userName:'susheel',
          passWord : 'susheel',
        }
        checkpassword = () => {
          if(this.userid.value == this.state.userName && this.pass.value == this.state.passWord )
          {
             history.push("/user");
             this.props.check();
          }
          else {
              this.userid.value='';
              this.pass.value='';
              alert("Incorrect Password!");
          }
        }

  render(){
    console.log(this.props.redirect)
    return (
        <div>
        <div className="Login-block">
          <div>
            <h1 className="Login-header">login</h1>
          </div>
          <div className="Login-form">
            <div className="Login-Row">
              <div className="Login-Col" > User ID</div>
              <div><input type="text" ref={(input)=>{this.userid = input}}/></div>
            </div>
            <div className="Login-Row">
              <div className="Login-Col" > Password</div>
              <div><input type="text" ref={(input)=>{this.pass = input}} /></div>
            </div>
            <div className="Login-Row">
              <button type="sumbit" onClick={this.checkpassword }>Login</button>
            </div>
          </div>
        </div>
  </div>
    );
  }
}
const mapStateToProps = (state) => {
  return{
    redirect : state.redirect
  }
};
const mapDispachToProps = (dispatch) => {
  return{
    check: () => dispatch({type:'CHECK_PASSWORD' })
  }
};

export default connect(mapStateToProps,mapDispachToProps)(Login);
