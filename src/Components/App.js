import React from "react";
import { Router, Route,Switch } from "react-router-dom";
import Login from './Login'
import history from "../history";
import User from './User.js'

const App = () => {
  return (
    <div className="ui container">
      <Router history={history}>

        <div>
          <Switch>
            <Route path="/" exact component={Login} />
            <Route path="/user" exact component={User} />

          </Switch>
        </div>
      </Router>
    </div>
  );
};

export default App;
